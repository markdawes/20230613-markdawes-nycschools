package com.rave.nycschools.model.local.entity

/**
 * Represents a School entity.
 *
 * @property numOfSatTestTakers The number of SAT test takers at the school.
 * @property satCriticalReadingAvgScore The SAT critical reading average score at the school.
 * @property satMathAvgScore The SAT math average score at the school.
 * @property satWritingAvgScore The SAT writing average score at the school.
 * @property schoolName The name of the school.
 */
data class School(
    val numOfSatTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String,
    val schoolName: String
)
