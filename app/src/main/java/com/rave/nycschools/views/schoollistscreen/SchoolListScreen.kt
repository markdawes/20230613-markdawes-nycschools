package com.rave.nycschools.views.schoollistscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.rave.nycschools.model.local.entity.School
import com.rave.nycschools.schoollist.SchoolListViewModel
import com.rave.nycschools.views.Screens

/**
 * Composable function representing the screen displaying the list of schools.
 *
 * @param viewModel The SchoolListViewModel instance for managing the state and data.
 * @param navigate The callback function for navigating to the detail screen.
 */
@Composable
fun SchoolListScreen(
    viewModel: SchoolListViewModel,
    navigate: (screen: Screens, school: School) -> Unit
) {
    val state = viewModel.state

    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            // Display the title "School List" at the center horizontally
            Text(
                text = "School List",
                style = MaterialTheme.typography.headlineLarge,
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            )
            Spacer(modifier = Modifier.height(16.dp)) // Space between title and list
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                // Render a SchoolCard for each school in the state
                items(state.schools) { school ->
                    SchoolCard(school) { selectedSchool ->
                        // Invoke the navigate callback to navigate to the detail screen
                        navigate(Screens.DetailScreen, selectedSchool)
                    }
                }
            }
        }
    }
}

/**
 * Composable function representing a card displaying information about a school.
 *
 * @param school The School object to display.
 * @param navigate The callback function for navigating to the detail screen.
 */
@Composable
fun SchoolCard(school: School, navigate: (School) -> Unit) {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .clickable { navigate(school) },
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
    ) {
        // Display the school name with the specified typography and color
        Text(
            text = school.schoolName,
            style = MaterialTheme.typography.bodyLarge,
            color = MaterialTheme.colorScheme.secondary,
            modifier = Modifier.padding(16.dp)
        )
    }
}
